# ansible_paloaltonetworks_testing
#

Running some basic timing tests with paloaltonetworks.panos galaxy collection in an AWX maintained environment

Getting facts on a single pa-220 with minimal configuration takes around 60 seconds.

This is not sustainable in a large environment.

This playbook details a way to work around gathering facts while still retaining a timestamp.

sources:

https://github.com/PaloAltoNetworks/pan-os-ansible

https://github.com/ansible/ansible

https://github.com/ansible/awx/tree/execution-environments