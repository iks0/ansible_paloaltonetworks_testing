#!/bin/bash
date --rfc-3339=seconds | sed 's/\ /_/g' | sed 's/\://g' | awk 'BEGIN {FS="-"}{print $1$2$3}' > .timestamp
echo "timestamp: $(cat .timestamp)" > .timestamped
